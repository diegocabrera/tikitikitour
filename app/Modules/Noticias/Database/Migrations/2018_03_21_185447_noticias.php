<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Noticias extends Migration
{
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
			$table->increments('id');
			$table->string('titulo', 250);
			$table->string('slug', 250)->unique();
			$table->string('audio',250)->nullable();
			$table->text('contenido');
			$table->text('contenido_html');
			$table->text('resumen');
			$table->integer('categoria_id')->unsigned();


			$table->timestamp('published_at')->nullable();

			$table->timestamps();
			$table->softDeletes();
            
            $table->foreign('categorias_id')
				->references('id')->on('categorias')
				->onDelete('cascade')->onUpdate('cascade');
		});
    }

    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
