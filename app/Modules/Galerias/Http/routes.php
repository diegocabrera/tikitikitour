<?php

Route::group(['middleware' => 'web', 'prefix' => 'galerias', 'namespace' => 'App\\Modules\Galerias\Http\Controllers'], function()
{
    Route::get('/', 'GaleriasController@index');
});
