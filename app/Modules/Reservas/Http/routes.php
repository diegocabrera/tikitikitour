<?php

Route::group(['middleware' => 'web', 'prefix' => 'reservas', 'namespace' => 'App\\Modules\Reservas\Http\Controllers'], function()
{
    Route::get('/', 'ReservasController@index');
});
