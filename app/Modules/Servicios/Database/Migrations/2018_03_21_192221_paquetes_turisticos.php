<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaquetesTuristicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes_turisticos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->string('ubicacion', 200);
            $table->timestamp('fecha_ini', 200);
            $table->timestamp('fecha_fin', 200);
            $table->timestamp('published_at', 200);
            $table->integer('hoteles_id')->unsigned()->nullable();
            $table->integer('cruceros_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

			$table->foreign('hoteles_id')
				->references('id')->on('hoteles')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('cruceros_id')
				->references('id')->on('cruceros')
				->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paquetes_turisticos');
    }
}
