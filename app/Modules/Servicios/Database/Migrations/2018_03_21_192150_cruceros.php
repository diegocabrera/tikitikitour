<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cruceros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruceros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->string('partida', 200);
            $table->string('llegada', 200);
            $table->timestamp('fecha_ini', 200);
            $table->timestamp('fecha_fin', 200);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cruceros');
    }
}
