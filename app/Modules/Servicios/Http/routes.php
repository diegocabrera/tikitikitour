<?php

Route::group(['middleware' => 'web', 'prefix' => 'servicios', 'namespace' => 'App\\Modules\Servicios\Http\Controllers'], function()
{
    Route::get('/', 'ServiciosController@index');
});
